;;;-----------------------------------------
;;; CS3520 Assignment 2 by Eric Platt
;;;-----------------------------------------


; Question 1
;-----------


(defun occurances (l)
  (if (null l)
    nil
    (occurances-h (cdr l) (car l) 1))
)

(defun occurances-h (l item value &optional tail)
  (if (null l)
    (cons (cons item value) (occurances tail))
    (if (eql (car l) item)
      (occurances-h (cdr l) item (+ value 1) tail)
      (occurances-h (cdr l) item value (push (car l) tail))
    )
  )
)

; Question 2
;-----------


; a)


(defun posPlus-recursive (l)
  (if (null l)
    nil
    (cons (car l) (posPlus-h (cdr l) 1))
  )
)

(defun posPlus-h (l &optional pos)
  (if (null l)
    nil
    (cons (+ (car l) pos) (posPlus-h (cdr l) (+ pos 1)))
  )
)


; b)


(defun posPlus-iterative (l)
  (setf counter 0)
  (setq newList '())
  (dolist (elm l)
    (setq newList (append newList (list (+ counter elm))))
    (setf counter (+ counter 1))
  )
  newList
)


; Question 3
;-----------


(defun rotate (arr)
  (if (eql (car (array-dimensions arr)) (cadr (array-dimensions arr)))
    (progn 
      (setq matrix '())
      (dotimes (i (car (array-dimensions arr)))
	(setq row '())
	(dotimes (j (car (array-dimensions arr)))
	  (push (aref arr j i) row)
	)
	(push row matrix)
	)
      )
    nil
  )
  (reverse matrix)
)



; Question 4
;-----------


; a)


(defun assocToHash (dots)
  (setq potato (make-hash-table))
  (dolist (elm dots)
    (setf (gethash (car elm) potato) (cdr elm))
  )
  potato
)

; b)


(defun hashToAssoc (potato)
  (setq dots '())
  (maphash #'(lambda (key value) 
	       (setq dots (append dots (list (cons key value))))
	     ) potato)
  dots
)


