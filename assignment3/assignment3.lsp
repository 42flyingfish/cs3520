; Assignment 3 by Eric Platt

(defun abstraction_sort (f)
  (lambda (l)
    (funcall f l)))


(defun selection_sort (l)
  (if (null l)
    l
    (let ((small (reduce #'min l)))
      (cons small (selection_sort (remove-element small l)))
      )))

(defun remove-element (n l)
  "Returns a list without the first occurance of n"
  (if (null l)
    l 
    (if (eql n (car l))
      (cdr l)
      (cons (car l) (remove-element n (cdr l)))
      )))


(defun insertion_sort (l)
  (if (null l)
    l
    (insertion_sort_helper l nil)
    ))

(defun insertion_sort_helper (l accumulator)
  (if (null l)
    accumulator
    (insertion_sort_helper (cdr l) (insert (car l) accumulator))
    ))

(defun insert (n l)
  (if (null l)
    (list n)
    (if (< n (car l))
      (cons n l)
      (cons (car l) (insert n (cdr l)))
      )))


; Demo of abstraction_sort in action

(setf sort (abstraction_sort 'selection_sort))
(write (funcall sort (list 12 3 5)))
(terpri)

(setf sort (abstraction_sort 'insertion_sort))
(write (funcall sort (list 12 3 5)))
(terpri)
