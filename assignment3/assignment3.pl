%%
%% Assignment 3 by Eric Platt

% name changed from sort so that swi prolog would stop complaining about sort
my_sort(Original, Sorted_List) :- abstraction_sort(Original, insertion_sort, Sorted_List).
my_sort(Original, Sorted_List) :- abstraction_sort(Original, selection_sort, Sorted_List).


% Where P is the sorting algorithm
abstraction_sort([], _, []).
abstraction_sort(Original, P, Answer) :- call(P, Original, Answer).


% insertion sort code largely based on blackboard

insertion_sort(Original, Sorted_List) :- sort_helper(Original, [], Sorted_List).

sort_helper([],L,S) :- S = L.
sort_helper([X|T],L,S) :- insert(X,L,R), sort_helper(T,R,S).

insert(X,[],S) :- S = [X].
insert(X,[Y|T],S) :- X < Y, !, L = [Y|T], S = [X|L].
insert(X,[Y|T],S) :- insert(X,T,R), S = [Y|R].


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% selection sort (all code original)
selection_sort([], L) :- L = [].
selection_sort(O, L) :- L = [H|N], min(O, [], H), remove(O, H, T), selection_sort(T, N). 

% Returns min value entry of a list
min([], X, X).
min([H|T], [], L) :- min(T, H, L).
min([H|T], X, L) :- H >= X, min(T, X, L).
min([H|T], X, L) :- H < X, min(T, H, L).



% Returns a list without the first occurance of a value
remove([H|T], Value, L) :-  L = T, Value is [H], !.
remove([H1, H2|T], Value, L) :-  L = [H1|N], remove([H2|T], Value, N).

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Demo of my_sort in action
:- my_sort([12, 3, 5], Z), writeln(Z).
:- my_sort([], Z), writeln(Z).
